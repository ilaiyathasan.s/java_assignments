package collection;

import java.util.TreeSet;

public class TreesetDemo {

	public static void main(String[] args) {
TreeSet<Integer> t = new TreeSet<Integer>();
t.add(10);
t.add(20);
t.add(30);
t.add(40);
t.add(50);
System.out.println(t);
boolean s = t.contains(20);
System.out.println(s);
t.remove(40);
System.out.println(t);
	}
	

}
