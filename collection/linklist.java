package collection;

import java.util.ArrayList;
import java.util.LinkedList;

public class linklist {

	public static void main(String[] args) {
		
		LinkedList<String> list = new LinkedList<String>();
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");
		list.add("f");
		System.out.println(list);

		list.add(1, "a1");
		System.out.println(list);
		list.addFirst("z");
		list.addLast("y");
		System.out.println(list);
		list.remove("d");
		System.out.println(list);

	}

}
