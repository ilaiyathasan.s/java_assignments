package com.sample;

public class Stringconcept{

	public static void main(String[] args) {
String s1="Welcome";//string constant pool area
char[]c= {'s','e','l','e','n','i','u','m'};
String s2= "Welcome";//string constant pool area
String s3= new String("Welcome");//Heap memory
String s4 = new String("Hello World");//Heap memory
String s5= new String(c);//heap memory
System.out.println(s5);
System.out.println(s1);
System.out.println(s2);
System.out.println(s3);
System.out.println(s4);
s1.concat("java");
System.out.println(s1);
s1=s1.concat("java");
System.out.println(s1);

	}

}
